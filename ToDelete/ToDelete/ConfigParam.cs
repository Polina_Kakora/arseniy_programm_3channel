﻿

namespace ToDelete
{
    public class ConfigParam
    {
        public string Ip { get; set; }
        public int Port { get; set; }
        public int RequestTime { get; set; }
        public double ScaleIncrement { get; set; }
        public bool IsTransposed { get; set; }

        public ConfigParam()
        { }

        public ConfigParam(string ip, int port, int requestTime, double scaleIncrement, bool isTransposed)
        {
            Ip = ip;
            Port = port;
            RequestTime = requestTime;
            ScaleIncrement = scaleIncrement;
            IsTransposed = isTransposed;
        }
    }
}
