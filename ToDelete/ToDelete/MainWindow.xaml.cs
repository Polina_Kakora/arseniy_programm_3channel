﻿using Arction.Wpf.Charting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using OfficeOpenXml;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ToDelete
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        // for tests
        #region forTests
        private CancellationTokenSource _tokenSource;
        private CancellationTokenSource _TesttokenSource;
        

        private const short minPoints = 10;
        private const short maxPoints = 10_000;

        private int _delay = 1000;
        private ushort _numOfTestPoints = 100;
        private Random _random = new Random();
        #endregion

        private TCPClient Client = new TCPClient(2, 1);
        

        Thread requestThread; //поток для таймера и отправки запроса на сервер
        private int timerForRequest_ms;
        
        public double answerTime = 0;

        #region Flags
        private bool isAutoFitView;
        private bool isTransposed;
        private bool isOnlyRE;
        private bool isScaleSet;
        private bool isLogCount;
        private bool isPowYCount;
        private bool isMaxholdActive;
        private bool isMinholdActive;
        private bool isSendingRequestStoped = true;
        #endregion

        ConfigParam settings;

        public double AnswerTime
        {
            get { return answerTime; }
            private set
            {
                answerTime = value;
                PropertyChanged(this, new PropertyChangedEventArgs("AnswerTime"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public MainWindow()
        {
            DataContext = this;
            
            string deploymentKey = "lgCAALq+gYUUUNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA5LTE5I1JldmlzaW9uPTACgD8RLUj4MtJqUZJYP08cAT2iWwf3J9OtBpdL7i2N+0kh7SWkWVA97OxGhM4wObsk67coGddfPr0up6PC3C0KPwwMCiXxkTBWdZ08iYj+WZzzt0Nh0WCA1IHun718ZKUQZyIfbWo+Zm/ye5a/SYJRwoenYVg95HKI3lUD+tAs9E5lNkeIgHiWrxUpQCFd+lN3d6SVnmaaRwMoLdT6iZF8bbI9drqJKFlcQX2RmV5CHt9ABh2AS4G8AbJMUHJrk4dxbyYxZwINYnlhMJPLtitabW+iK+ZQel62jiAm7jPlCGuWUEX34UUVZkOov5jUOIfYuIlgeDLbVNrcdWXOyy34b/+/D5WBCfBrbbINSOFtti+asDiZzEK6nbPL4FU90A14EOFoY68fsAkM3mdae2V0Kn42zZ0imxz84lfwsYfgjDO7MLwKmeU8YSJQJgRTL4A7bJl72NmDGlBnoPCOvDiTOe3xzEKwBle0yNbQfvvOJ47mwibGxo0gxv/o78RSq0SaaMs=";
            LightningChartUltimate.SetDeploymentKey(deploymentKey);

            InitializeComponent();
            InitFlagsByInterface();
            InitCharts();
            InitTextBoxValues();

            checkBoxLogX.RaiseEvent(new RoutedEventArgs(CheckBox.CheckedEvent)); //?
            checkBoxLogY.RaiseEvent(new RoutedEventArgs(CheckBox.CheckedEvent));

            Client.TryToGetAllPackets += Client_TryToGetAllPackets;
        }


        private void Client_TryToGetAllPackets(object sender, EventArgs e)
        {
            SendRequest();
        }


        private void InitFlagsByInterface()
        {
            isLogCount = (bool)checkBoxLogY.IsChecked;
            isPowYCount = (bool)checkBoxPowY.IsChecked;
            isAutoFitView = (bool)checkBoxFitView.IsChecked;
            isScaleSet = (bool)checkBoxScale.IsChecked;
            isTransposed = (bool)checkBoxReplace.IsChecked;
            isOnlyRE = (bool)checkBoxOnlyRE.IsChecked;
            isMaxholdActive = (bool)checkBoxMaxHold.IsChecked;
            isMinholdActive = (bool)checkBoxMinHold.IsChecked;
        }


        private void InitTextBoxValues()
        {
            settings = YamlLoad();
            IPTextBox.Text = settings.Ip;
            PortTextBox.Text = settings.Port.ToString();
            tbRequestTimer.Text = settings.RequestTime.ToString();
            checkBoxReplace.IsChecked = settings.IsTransposed;
        }



        #region Test mode
        private List<double> CreateData()
        {
            var outputData = new List<double>();
            for (int j = 0; j < _numOfTestPoints; j++)
            {
                outputData.Add(_random.Next(0, 360));
            }
            return outputData;
        }


        private async Task WorkTask(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                Dispatcher?.Invoke(() =>
                {
                    for (byte i = 0; i < 3; i++)
                    {
                        UpDateChart((byte)(i+1), CreateData());
                    }
                });
                await Task.Delay(_delay);
            }
        }
        #endregion



        #region Buttons
        private void BtnConnect_Click(object sender, RoutedEventArgs e)
        {
            string strIP;
            int testPortOperChannel;
            if (IPTextBox.Text.Equals(""))
            {
                strIP = "127.0.0.1";
                IPTextBox.Text = "127.0.0.1";
            }
            else
            {
                strIP = IPTextBox.Text;
            }

            if (PortTextBox.Text.Equals(""))
            {
                testPortOperChannel = 11111;
                PortTextBox.Text = testPortOperChannel.ToString();
            }
            else
            {
                testPortOperChannel = Convert.ToInt32(PortTextBox.Text);
            }

            if (Client.Connect(strIP, testPortOperChannel))
            {
                Client.OnConnectNet += Connected;
                Client.OnDisconnectNet += Disconnected;
                Client.OnReadByte += MathFunc;
                Client.OnWriteByte += WritedBytes;

                ConnectButton.Background = Brushes.Green;
                DisconnectButton.Background = Brushes.Gray;
            }
            else
            {
                ConnectButton.Background = Brushes.Gray;
                DisconnectButton.Background = Brushes.Red;
            }
        }


        private void BtnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            Client.OnConnectNet -= Connected;
            Client.OnDisconnectNet -= Disconnected;
            Client.OnReadByte -= MathFunc;
            Client.OnWriteByte -= WritedBytes;

            Client.Disconnect();
            DisconnectButton.Background = Brushes.Red;
            ConnectButton.Background = Brushes.Gray;
        }


        private void BtnCleanAll_Click(object sender, RoutedEventArgs e)
        {
            CleanCharts();
        }


        private void BtnFitView_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < NumOfCharts; i++)
                chartArray[i].ViewXY.ZoomToFit();
        }


        private void BtnTest_Click(object sender, RoutedEventArgs e)
        {
            if (_tokenSource != null && !_tokenSource.Token.IsCancellationRequested)
            {
                _tokenSource.Cancel();
            }
            else
            {
                InitChartsForTest();
                _tokenSource = new CancellationTokenSource();
                WorkTask(_tokenSource.Token);
                //button.Content = "Stop";
                for (int i = 0; i < NumOfCharts; i++)
                    chartArray[i].ViewXY.PointLineSeries[0].SeriesEventMarkers.RemoveRange(0, chartArray[i].ViewXY.PointLineSeries[0].SeriesEventMarkers.Count);
            }
        }


        private void BtnSendRequest_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_TesttokenSource != null && !_TesttokenSource.Token.IsCancellationRequested)
                {
                    isSendingRequestStoped = true;
                    requestThread?.Abort();
                    SaveExcel(savedLastPoints);
                    _TesttokenSource.Cancel();
                    SendRequestButton.Content = "Send request";
                    //FitView();
                }
                else
                {
                    isSendingRequestStoped = false;
                    _TesttokenSource = new CancellationTokenSource();
                    timerForRequest_ms = Convert.ToInt32(tbRequestTimer.Text);
                    SendRequest();
                    SendRequestButton.Content = "Stop";
                    YamlSave(new ConfigParam( IPTextBox.Text, Convert.ToInt32(PortTextBox.Text), Convert.ToInt32(tbRequestTimer.Text), settings.ScaleIncrement, (bool)checkBoxReplace.IsChecked) );
                    //FitView();
                }
            }
            catch (Exception ex)
            { }
        }

        private void BtnSendCode_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var code = byte.Parse(tbCode.Text, System.Globalization.NumberStyles.HexNumber);

                if (code < 0x00 || code > 0xff)
                { return; }

                SendRequest(code);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion



        private void StartTimer()
        {
            if (!isSendingRequestStoped)
            {
                Thread.Sleep(timerForRequest_ms);
                SendRequest();
            }
        }
        


        #region EventHandlers
        private void Connected()
        {
            Dispatcher.Invoke(() => ConnectButton.Background = Brushes.Green);
            Dispatcher.Invoke(() => DisconnectButton.Background = Brushes.Gray);
        }


        private void Disconnected()
        {
            Dispatcher.Invoke(() => ConnectButton.Background = Brushes.Gray);
            Dispatcher.Invoke(() => DisconnectButton.Background = Brushes.Red);
        }


        private void WritedBytes(byte[] bByte)
        {
        }


        //prepare data for charts
        private void MathFunc(byte[] resultBData)
        {
            try
            {
                AnswerTime = Client.countTime;
                //requestThread.Abort();
                if (resultBData.Count() >= 1)
                {
                    List<double> parts = new List<double>();
                    List<int> partsTemp = new List<int>();
                    byte channel = resultBData[0];
                    double temp;

                    if (isOnlyRE)
                    {
                        for (int i = 1; i < resultBData.Length; i += 4)
                        {
                            temp = Convert.ToDouble( BitConverter.ToInt16(resultBData, i));
                            parts.Add(temp);
                        }
                        savedLastPoints[channel - 1] = new List<double>(parts);
                        Dispatcher?.Invoke(() =>
                        {
                            UpDateChart(channel, parts);
                        });
                        return;
                    }
                    
                    
                    for (int i = 1; i < resultBData.Length; i += 4)
                    {
                        int tempInt = BitConverter.ToInt32(resultBData, i);
                        if (tempInt == 0)
                        {
                            tempInt = 1;
                        }
                        temp = Convert.ToDouble(tempInt);
                        if (isScaleSet)
                        {
                            temp *= settings.ScaleIncrement;
                            //temp *= (1.379 * Math.Pow(10, -3));
                        }

                        if (isLogCount)
                        {
                            
                            temp = 20 * Math.Log10(temp);
                        }

                        if (isPowYCount)
                        {
                            temp = 10 * Math.Log10(temp * temp / (50 * 0.001));
                        }
                        parts.Add(temp);
                        partsTemp.Add(tempInt);
                    }

                    if (isTransposed)
                    {
                        var newList = new List<double>();
                        newList = parts.GetRange(parts.Count / 2, parts.Count / 2);
                        newList.AddRange(parts.GetRange(0, parts.Count / 2));
                        savedLastPoints[channel-1] = new List<double>(newList);
                        Dispatcher?.Invoke(() =>
                        {
                            UpDateChart(channel, newList);
                        });
                    }
                    else
                    {
                        savedLastPoints[channel - 1] = new List<double>(parts);
                        Dispatcher?.Invoke(() =>
                        {
                            UpDateChart(channel, parts);
                        });
                    }

                    if (channel == 2)
                    {
                        Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                        {
                            CalculateDifference();
                        });
                    }
                }
            }
            catch (Exception exp) { }
        }
        #endregion


        private void CalculateDifference()
        {
            var newList = new List<double>();

            if (savedLastPoints[0].Count == savedLastPoints[1].Count)
            {
                for (int i = 0; i < savedLastPoints[0].Count; i++)
                {
                    newList.Add(savedLastPoints[0][i] - savedLastPoints[1][i]);
                }
            }

            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                UpDateChart(3, newList);
            });
        }


        private async void SendRequest()
        {
            byte[] test = new byte[8];
            test[0] = 0x12;
            test[1] = 0x01;
            test[2] = 0x00;
            test[3] = 0x01;
            test[4] = 0x01;
            test[5] = 0x00;
            test[6] = 0x00;
            int sum = 0;
            for (int i = 0; i < 7; i++)
            {
                sum += test[i];
            }
            test[7] = Convert.ToByte(sum % 255);
            
            Client.SendBytesWithoutPacket(test);
            await Task.Delay(10);
        }

        private async void SendRequest(byte code)
        {
            byte[] test = new byte[8];
            test[0] = 0x12;
            test[1] = code;
            test[2] = 0x00;
            test[3] = 0x01;
            test[4] = 0x01;
            test[5] = 0x00;
            test[6] = 0x00;
            int sum = 0;
            for (int i = 0; i < 7; i++)
            {
                sum += test[i];
            }
            test[7] = Convert.ToByte(sum % 255);

            Client.SendBytesWithoutPacket(test);
            await Task.Delay(10);
        }





        /// <summary>
        /// Dispose items in collection before and clear.
        /// </summary>
        /// <typeparam name="T">Collection type</typeparam>
        /// <param name="list">Collection</param>
#if WpfSemibindable
        public static void DisposeAllAndClear<T>(System.Windows.FreezableCollection<T> list) where T : System.Windows.Freezable
#else
        public static void DisposeAllAndClear<T>(System.Windows.FreezableCollection<T> list) where T : System.Windows.Freezable
#endif
        {
            if (list == null)
                return;

            foreach (IDisposable item in list)
            {
                if (item != null)
                    item.Dispose();
            }
            list.Clear();
        }





        #region CheckBoxes
        private void checkBoxLogX_CheckedChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (checkBoxLogX.IsChecked == true)
                {
                    for (int i = 0; i < NumOfCharts; i++)
                        chartArray[i].ViewXY.XAxes[0].ScaleType = ScaleType.Logarithmic;
                }
                else
                {
                    for (int i = 0; i < NumOfCharts; i++)
                        chartArray[i].ViewXY.XAxes[0].ScaleType = ScaleType.Linear;
                }
            }
            catch { }
        }

        
        private void checkBoxLogY_CheckedChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (checkBoxLogY.IsChecked == true)
                {
                    isLogCount = true;
                    checkBoxScale.IsChecked = true;
                    checkBoxPowY.IsChecked = false;
                }
                else
                {
                    isLogCount = false;
                }
            }
            catch { }
        }


        private void checkBoxPowY_CheckedChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (checkBoxPowY.IsChecked == true)
                {
                    isPowYCount = true;
                    checkBoxLogY.IsChecked = false; //возможно заменить значение флага, если не вызывается событие
                    checkBoxScale.IsChecked = true;
                }
                else
                {
                    isPowYCount = false;
                    
                }
            }
            catch { }
        }


        private void CheckBoxFitView_Checked(object sender, RoutedEventArgs e)
        {
            if (checkBoxFitView.IsChecked == true)
            {
                isAutoFitView = true;
            }
            else
            {
                isAutoFitView = false;
            }
        }


        private void CheckBoxScale_Checked(object sender, RoutedEventArgs e)
        {
            if (chartArray != null)
            {
                ResetAccumulations();
            }

            if (checkBoxScale.IsChecked == true)
            {
                isScaleSet = true;
            }
            else
            {
                isScaleSet = false;
            }
        }


        private void CheckBoxReplace_Checked(object sender, RoutedEventArgs e)
        {
            if (chartArray != null)
            {
                ResetAccumulations();
            }
            if (checkBoxReplace.IsChecked == true)
            {
                isTransposed = true;
            }
            else
            {
                isTransposed = false;
            }
        }


        private void CheckBoxOnlyRE_Checked(object sender, RoutedEventArgs e)
        {
            if (chartArray != null)
            {
                ResetAccumulations();
            }

            if (checkBoxOnlyRE.IsChecked == true)
            {
                isOnlyRE = true;
                checkBoxPowY.IsChecked = false;
            }
            else
            {
                isOnlyRE = false;
            }
        }


        private void CheckBoxMaxHold_Checked(object sender, RoutedEventArgs e)
        {
            if (chartArray != null)
            {
                ResetAccumulations();
            }

            if (checkBoxMaxHold.IsChecked == true)
            {
                isMaxholdActive = true;
            }
            else
            {
                isMaxholdActive = false;
            }
        }


        private void CheckBoxMinHold_Checked(object sender, RoutedEventArgs e)
        {
            if (chartArray != null)
            {
                ResetAccumulations();
            }

            if (checkBoxMinHold.IsChecked == true)
            {
                isMinholdActive = true;
            }
            else
            {
                isMinholdActive = false;
            }
        }
        #endregion



        private void InitChartsForTest()
        {
            try
            {
                var newValue = (ushort)Int32.Parse(tbPoints.Text);
                if (newValue >= minPoints && newValue <= maxPoints)
                {
                    _numOfTestPoints = newValue;
                    if (!isTransposed)
                    {
                        for (int i = 0; i < NumOfCharts; i++)
                        {
                            chartArray[i].ViewXY.XAxes[0].SetRange(0, _numOfTestPoints);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < NumOfCharts; i++)
                        {
                            chartArray[i].ViewXY.XAxes[0].SetRange(-_numOfTestPoints / 2, _numOfTestPoints / 2);
                        }
                    }
                }
            }
            catch
            {
                tbPoints.Text = _numOfTestPoints.ToString();
            }
        }



        private void Window_Closed(object sender, EventArgs e)
        {
            Client.OnConnectNet -= Connected;
            Client.OnDisconnectNet -= Disconnected;
            Client.OnReadByte -= MathFunc;
            Client.OnWriteByte -= WritedBytes;

            Client.Disconnect();
        }
        

        private void SaveExcel(List<double>[] data)
        {
            try
            {
                //запись в Exel
                using (var excel = new ExcelPackage())
                {

                    var ws = excel.Workbook.Worksheets.Add("MyWorksheet");
                    if (data[0].Count != 0)
                    {
                        for (int i = 0; i < data[0].Count; i++)
                        {
                            ws.Cells["A" + (i + 1).ToString()].Value = data[0][i];
                        }
                    }
                    if (data[1].Count != 0)
                    {
                        for (int i = 0; i < data[1].Count; i++)
                        {
                            ws.Cells["B" + (i + 1).ToString()].Value = data[1][i];
                        }
                    }
                    excel.SaveAs(new FileInfo("test.xlsx"));
                }
            }
            catch(Exception e) { }
        }


    }
}
