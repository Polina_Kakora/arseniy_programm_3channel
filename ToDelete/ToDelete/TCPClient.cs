﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ToDelete
{

    public struct Packet
    {
        public PacketServiceData packetServiceData;
        public byte[] bData;

        private static ushort test = 2;
    }

    public struct PacketServiceData
    {
        public byte bAdressSender;
        public byte bAdressReceiver;
        public byte bCode;
        public byte bCounter;
        public ushort usLengthInform;
    }


    public class TCPClient
    {
        #region variables

        private Thread thrRead;
        public NetworkStream streamClient;
        private TcpClient tcpClient;
        private byte bAdrOwn = 0;
        private byte bAdrOpponent = 0;
        private byte COUNTER_CMD = 0;

        #endregion

        const int LEN_HEAD = 7;
        const int LEN_ADDITIONAL = 8;

        #region constants
        const byte CONNECTION_REQUEST_CODE = 9;
        const byte TEST_MESSAGE_CODE = 10;
        const byte TEXT_MESSAGE_APPROVED_CMD_CODE = 11;
        const byte EXEC_BEAR_CODE = 12;
        const byte FRIEQUENCIES_FOR_SUPPRESSION_CODE = 13;
        const byte COORD_REQUEST_CODE = 14;

        const byte VALUE_OF_MESSAGE_APPROVED_TEXT_CMD = 0;

        #endregion



        //************** EVENT **************//      
        public delegate void ConnectEventHandler();
        public event ConnectEventHandler OnConnectNet;
        public event ConnectEventHandler OnDisconnectNet;

        public delegate void ByteEventHandler(byte[] bByte);
        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler OnWriteByte;

        public delegate void CmdConnectionRequestEventHandler(byte bRegime);
        public event CmdConnectionRequestEventHandler OnConnectionRequest;

        public event EventHandler TryToGetAllPackets; //кидать при получении >=1024 байт.

        public DateTime sentRequest;
        public DateTime gotAnswer;
        public double countTime;

        public TCPClient()
        {
            //NumsOfPacketsList = new List<byte>();
            
        }

        // connect to server
        protected virtual void ConnectNet()
        {
            if (OnConnectNet != null)
            {
                OnConnectNet();//Raise the event
                PacketsCollection = new Dictionary<byte, byte[]>();
            }
        }

        // disconnect 
        protected virtual void DisconnectNet()
        {
            if (OnDisconnectNet != null)
            {
                OnDisconnectNet();
            }
        }

        // read array of byte
        protected virtual void ReadByte(byte[] bByte)
        {
            if (OnReadByte != null)
            {
                OnReadByte(bByte);
            }

        }

        // write array of byte
        protected virtual void WriteByte(byte[] bByte)
        {
            if (OnWriteByte != null)
            {
                OnWriteByte(bByte);
            }
        }

        // read connection request
        protected virtual void ConnectionRequest(byte bRegime)
        {
            if (OnConnectionRequest != null)
            {
                OnConnectionRequest(bRegime);
            }
        }

        // constructor
        public TCPClient(byte AdrOwn, byte AdrOpponent)
        {
            bAdrOwn = AdrOwn;
            bAdrOpponent = AdrOpponent;
        }


        // connect to server
        public bool Connect(string strIPServer, int iPortServer)
        {
            // if there is client
            if (tcpClient != null)
            {
                tcpClient.Close();
            }

            // create client
            tcpClient = new TcpClient();

            try
            {
                // begin connect 
                var result = tcpClient.BeginConnect(strIPServer, iPortServer, null, null);

                // try connect
                var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1));

                if (!success)
                {
                    throw new Exception("Failed to connect.");
                }

                // we have connected
                tcpClient.EndConnect(result);
            }
            catch (Exception)
            {
                //generate event
                DisconnectNet();
                // logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                return false;
            }


            if (tcpClient.Connected)
            {
                // create stream for client
                if (streamClient != null)
                    streamClient = null;
                streamClient = tcpClient.GetStream();

                // destroy thread for reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // create thread for reading
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }

                catch (System.Exception)
                {

                    // generate event
                    DisconnectNet();
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                    return false;

                }
            }

            ConnectNet();
            return true;
        }


        public void Disconnect()
        {
            // if there is client
            if (streamClient != null)
            {
                // close client stream
                try
                {
                    streamClient.Close();
                    streamClient = null;
                }
                catch (System.Exception)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                }

            }

            // if there is client
            if (tcpClient != null)
            {
                // close client connection
                try
                {
                    tcpClient.Close();
                    tcpClient = null;
                }
                catch (System.Exception)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                }

            }

            // if there is client
            if (thrRead != null)
            {
                // destroy thread for reading
                try
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }
                catch (System.Exception)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                }

            }

            // generate event
            DisconnectNet();
        }


        private void CheckPacket(byte [] data)
        {
            try
            {
                if (PacketsCollection == null)
                {
                    PacketsCollection = new Dictionary<byte, byte[]>();
                }
                var code = data[1];

                if (code != 1)
                    return;

                var channel = data[2];
                var packetNum = data[3];
                totalPackets = data[4];
                //Array.Resize(ref data, data.Length - 1); 
                Array.Reverse(data, 5, 2);
                var dataToDraw = new byte[BitConverter.ToInt16(data, 5)];
                Array.Copy(data, 7, dataToDraw, 0, dataToDraw.Length);
                PacketsCollection.Add(packetNum, dataToDraw);
                if (!CheckCollectingComplete())
                {
                    TryToGetAllPackets?.Invoke(this, EventArgs.Empty);
                }
                else
                {
                    ReadByte(FormArrayToDraw(channel));
                }
            }
            catch (Exception e)
            { }
        }


        private bool CheckCollectingComplete()
        {
            if (PacketsCollection.Count < totalPackets)
            {
                return false;
            }

            
            return true;
        }


        private byte[] FormArrayToDraw(byte channel)
        {
            byte[] arrayToDraw = new byte[1];
            arrayToDraw[0] = channel;
            int oldLength = arrayToDraw.Length;
            for (byte i = 1; i <= PacketsCollection.Count; i++)
            {
                oldLength = arrayToDraw.Length;
                Array.Resize(ref arrayToDraw, oldLength + (PacketsCollection[i].Length));
                Array.Copy(PacketsCollection[i], 0, arrayToDraw, oldLength, PacketsCollection[i].Length);
            }
            PacketsCollection.Clear();
            return arrayToDraw;
        }


        static Dictionary<byte, byte[]> PacketsCollection;
        private static byte totalPackets = 0;


        private void ReadData()
        {
            int iReadLength = 0;
            int currSize = 0;
            while (true)
            {
                try
                {
                    
                    var buffer = new byte[LEN_HEAD];
                    
                    currSize = 0;
                    iReadLength = streamClient.Read(buffer, 0, LEN_HEAD);
                    //тут остановился до прихода сообщений

                    var packetSizeField = new byte[2];
                    Array.Copy(buffer, 5, packetSizeField, 0, 2);
                    short realPacketSize = BitConverter.ToInt16( packetSizeField.Reverse().ToArray() , 0);
                    

                    var bRead = new byte[realPacketSize + LEN_ADDITIONAL];
                    Array.Resize(ref buffer, realPacketSize + LEN_ADDITIONAL);

                    Array.Copy(buffer, 0, bRead, 0, LEN_ADDITIONAL);
                    
                    currSize += iReadLength;
                    
                    while (currSize < bRead.Length)
                    {
                        iReadLength = streamClient.Read(buffer, 0, bRead.Length - currSize);
                        Array.Copy(buffer, 0, bRead, currSize, iReadLength);
                        //var savedLength = CheckFirstPackets(buffer);
                        currSize += iReadLength;

                        if (iReadLength == 0)
                        {
                            Disconnect();
                        }
                    }
                    if (bRead.Length > 0)
                    {
                        
                        gotAnswer = DateTime.Now;
                        //считаем время задержки ответа
                        countTime = (gotAnswer - sentRequest).TotalMilliseconds;
                        CheckPacket(bRead);

                        //ReadByte(bRead);




                        //try
                        //{

                        //Packet packet = new Packet();
                        //byte[] bData = null;
                        //    if (bRead.Length >= LEN_HEAD)
                        //    {

                        //        //packet.packetServiceData.bAdressSender = bRead[0];
                        //        //packet.packetServiceData.bAdressReceiver = bRead[1];
                        //        packet.packetServiceData.bCode = bRead[0];
                        //        //packet.packetServiceData.bCounter = bRead[3];
                        //        // REVERSE THAT BITES
                        //        //packet.packetServiceData.usLengthInform = BitConverter.ToUInt16(bRead, 1);
                        //        packet.packetServiceData.usLengthInform = realPacketSize;

                        //        Array.Resize(ref bData, bRead.Length - LEN_HEAD);
                        //        Array.Copy(bRead, LEN_HEAD, bData, 0, bRead.Length - LEN_HEAD);
                        //        packet.bData = bData;

                        //        switch (packet.packetServiceData.bCode)
                        //        {
                        //            case CONNECTION_REQUEST_CODE:
                        //                byte bRegime = packet.bData[0];
                        //                ConnectionRequest(bRegime);
                        //                break;
                        //            case TEST_MESSAGE_CODE:
                        //                byte btest = packet.bData[0];
                        //                ConnectionRequest(btest);
                        //                //
                        //                break;
                        //            default:
                        //                break;
                        //        }
                        //    }
                        //}
                        //catch (System.Exception ex)
                        //{
                        //    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                        //}
                    }
                }
                catch (System.Exception ex)
                {
                    //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
                    DisconnectNet();
                    return;
                }
            }
        }

        ////example of codogramm(send something)
        //public void SendData(Packet packetToSend)
        //{
        //    byte[] bSend;
        //    byte[] bHead = Serializer.StructToByteArray(packetToSend.packetServiceData);
        //    if (packetToSend.bData != null)
        //    {
        //        bSend = new byte[bHead.Length + packetToSend.bData.Length];
        //        Array.Copy(bHead, 0, bSend, 0, bHead.Length);
        //        Array.Copy(packetToSend.bData, 0, bSend, LEN_HEAD, packetToSend.bData.Length);
        //    }
        //    else
        //        bSend = new byte[bHead.Length];
        //    Array.Copy(bHead, 0, bSend, 0, bHead.Length);


        //    if (COUNTER_CMD == 255)
        //    {
        //        COUNTER_CMD = 0;
        //    }

        //    WriteData(bSend);

        //}

        //Write bytes to port.


        private bool WriteData(byte[] bSend)
        {
            try
            {
                streamClient.Write(bSend, 0, bSend.Length);
                OnWriteByte(bSend);
            }
            catch (System.Exception)
            {
                return false;
            }

            return true;
        }

        public void SendBytesWithoutPacket(byte[] bSend)
        {
            try
            {
                // byte[] bSend = Serializer.StringToByteArray(strText);
                WriteData(bSend);
                sentRequest = DateTime.Now;
            }
            catch (System.Exception)
            {
                //logger.Error(ex, "   " + this.ToString() + ":  " + ex);
            }
        }
    }
}
