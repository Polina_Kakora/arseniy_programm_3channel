﻿using System;
using System.IO;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.TypeResolvers;

namespace ToDelete
{
    public partial class MainWindow
    {
        public ConfigParam YamlLoad()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader("Settings.yaml", System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var localProperties = new ConfigParam();
            try
            {
                localProperties = deserializer.Deserialize<ConfigParam>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (localProperties == null)
            {
                localProperties = GenerateDefaultLocalProperties();
                YamlSave(localProperties);
            }
            return localProperties;
        }

        private ConfigParam GenerateDefaultLocalProperties()
        {
            var localProperties = new ConfigParam();
            localProperties.Port = 7;
            localProperties.Ip = "192.168.1.10";
            localProperties.RequestTime = 20;
            localProperties.ScaleIncrement = 0.000349;
            localProperties.IsTransposed = true;
            return localProperties;
        }

        public T YamlLoad<T>(string NameDotYaml) where T : new()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(NameDotYaml, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var t = new T();
            try
            {
                t = deserializer.Deserialize<T>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //if (t == null)
            //{
            //    t = new T();
            //    YamlSave(t, NameDotYaml);
            //}
            return t;
        }

        public void YamlSave(ConfigParam localProperties)
        {
            try
            {
                var serializer = new SerializerBuilder().WithTypeResolver(new StaticTypeResolver()).Build();

                var yaml = serializer.Serialize(localProperties);
                string key = "DependencyObjectType";
                int pos = yaml.IndexOf(key);
                if (pos != -1)
                    yaml = yaml.Replace(yaml.Substring(pos), "");

                using (StreamWriter sw = new StreamWriter("Settings.yaml", false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void YamlSave<T>(T t, string NameDotYaml) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().ConfigureDefaultValuesHandling(DefaultValuesHandling.Preserve).Build();
                var yaml = serializer.Serialize(t);

                using (StreamWriter sw = new StreamWriter(NameDotYaml, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
