﻿using Arction.Wpf.Charting;
using Arction.Wpf.Charting.Axes;
using Arction.Wpf.Charting.EventMarkers;
using Arction.Wpf.Charting.SeriesXY;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Arction.Wpf.Charting.Views.ViewXY;

namespace ToDelete
{
    public partial class MainWindow
    {
        private static LightningChartUltimate[] chartArray;

        private const int NumOfCharts = 3;

        private SeriesEventMarker prevMarkerMain;
        private SeriesEventMarker prevMarkerMax;
        private SeriesEventMarker prevMarkerMin;

        private static List<double>[] savedLastPoints;
        private static List<double>[] maxHoldPoints;
        private static List<double>[] minHoldPoints;


        #region CreateChart
        /// <summary>
        /// Create chart
        /// </summary>
        private LightningChartUltimate CreateChart(Grid grid)
        {
            var chart = new LightningChartUltimate();
            //Disable rendering, strongly recommended before updating chart properties
            chart.BeginUpdate();

            AxisX xAxis = chart.ViewXY.XAxes[0];
            chart.Title.Visible = false;

            //Reduce memory usage and increase performance
            chart.ViewXY.DropOldSeriesData = false;
            //Don't show legend box
            chart.ViewXY.LegendBoxes[0].Visible = false;

            xAxis.SetRange(0, 90);
            xAxis.ScrollMode = XAxisScrollMode.Scrolling;
            xAxis.ValueType = AxisValueType.Number;
            chart.ViewXY.AxisLayout.YAxesLayout = YAxesLayout.Layered;

            //Remove existing y-axes
            foreach (AxisY yaxis in chart.ViewXY.YAxes)
                yaxis.Dispose();
            chart.ViewXY.YAxes.Clear();

            //Create new logaritmic y-axis and add to chart
            AxisY yAxis = new AxisY(chart.ViewXY);
            yAxis.Title.DistanceToAxis = 30;
            yAxis.SetRange(0, 40);
            yAxis.MinorGrid.Visible = true;
            chart.ViewXY.YAxes.Add(yAxis);

            chart.ViewXY.PointLineSeries.Add(CreateLine(chart.ViewXY, xAxis, yAxis, LineType.Main));
            chart.ViewXY.PointLineSeries.Add(CreateLine(chart.ViewXY, xAxis, yAxis, LineType.MaxHold));
            chart.ViewXY.PointLineSeries.Add(CreateLine(chart.ViewXY, xAxis, yAxis, LineType.MinHold));

            //Allow chart rendering
            chart.EndUpdate();

            grid.Children.Add(chart);

            chart.MouseMove += OnMouseMove;

            return chart;
        }


        private PointLineSeries CreateLine(ViewXY owner, AxisX xAxis, AxisY yAxis, LineType lineType)
        {
            //Create new PointLineSeries
            PointLineSeries pointLineSeries = new PointLineSeries(owner, xAxis, yAxis);
            pointLineSeries.MouseInteraction = false;
            pointLineSeries.MouseHighlight = MouseOverHighlight.Simple;

            pointLineSeries.Title.Visible = false;
            pointLineSeries.Title.Color = pointLineSeries.LineStyle.Color;

            pointLineSeries.LineStyle.AntiAliasing = LineAntialias.Normal;
            pointLineSeries.LineStyle.Width = 1;

            pointLineSeries.PointStyle.Color1 = Color.FromArgb(0, 255, 0, 0);
            //pointLineSeries.PointStyle.Color2 = Colors.Red;
            pointLineSeries.PointStyle.Width = 7;
            pointLineSeries.PointStyle.Height = 7;
            pointLineSeries.PointStyle.BorderWidth = 1;

            switch (lineType)
            {
                case LineType.Main:
                    pointLineSeries.LineStyle.Color = Colors.Yellow;
                    pointLineSeries.PointStyle.Width = 7;
                    pointLineSeries.PointStyle.Height = 7;
                    pointLineSeries.PointStyle.BorderWidth = 1;
                    break;
                case LineType.MaxHold:
                    pointLineSeries.LineStyle.Color = Colors.Gray;
                    pointLineSeries.PointStyle.Width = 3;
                    pointLineSeries.PointStyle.Height = 3;
                    pointLineSeries.PointStyle.BorderWidth = 0;
                    break;
                case LineType.MinHold:
                    pointLineSeries.LineStyle.Color = Colors.Blue;
                    pointLineSeries.PointStyle.Width = 3;
                    pointLineSeries.PointStyle.Height = 3;
                    pointLineSeries.PointStyle.BorderWidth = 0;
                    break;
            }

            return pointLineSeries;
        }


        private enum LineType
        {
            Main,
            MaxHold,
            MinHold
        }
        #endregion


        #region mouseMove

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            LightningChartUltimate chart = sender as LightningChartUltimate;
            chart.BeginUpdate();

            var point = e.GetPosition(chart);

            double x, y, xMax, yMax, xMin, yMin;
            int nearestIndex = 0;
            bool solved = chart.ViewXY.PointLineSeries[0].SolveNearestDataPointByCoord((int)point.X, (int)point.Y, out x, out y, out nearestIndex);
            bool solvedMax = chart.ViewXY.PointLineSeries[1].SolveNearestDataPointByCoord((int)point.X, (int)point.Y, out xMax, out yMax, out nearestIndex);
            bool solvedMin = chart.ViewXY.PointLineSeries[2].SolveNearestDataPointByCoord((int)point.X, (int)point.Y, out xMin, out yMin, out nearestIndex);


            if (solved)
            {
                var marker = CreateMouseMarkerMain(x, y, chart.ViewXY.PointLineSeries[0]);
                chart.ViewXY.PointLineSeries[0].SeriesEventMarkers.Add(marker);
                chart.ViewXY.PointLineSeries[0].SeriesEventMarkers.RemoveRange(0, chart.ViewXY.PointLineSeries[0].SeriesEventMarkers.Count - 1);

                if (isMaxholdActive)
                {
                    var markerMax = CreateMouseMarkerMax(xMax, yMax, chart.ViewXY.PointLineSeries[1]);
                    chart.ViewXY.PointLineSeries[1].SeriesEventMarkers.Add(markerMax);
                    chart.ViewXY.PointLineSeries[1].SeriesEventMarkers.RemoveRange(0, chart.ViewXY.PointLineSeries[1].SeriesEventMarkers.Count - 1);
                }

                if (isMinholdActive)
                {
                    var markerMin = CreateMouseMarkerMax(xMin, yMin, chart.ViewXY.PointLineSeries[2]);
                    chart.ViewXY.PointLineSeries[2].SeriesEventMarkers.Add(markerMin);
                    chart.ViewXY.PointLineSeries[2].SeriesEventMarkers.RemoveRange(0, chart.ViewXY.PointLineSeries[2].SeriesEventMarkers.Count - 1);
                }
            }
            chart.EndUpdate();
        }


        private SeriesEventMarker CreateMouseMarkerMain(double x, double y, SeriesBaseXY owner)
        {
            SeriesEventMarker sem = new SeriesEventMarker(owner);
            sem.XValue = x;
            sem.YValue = y;

            //if ((!(prevMarkerMain != null) || (prevMarkerMain.XValue != sem.XValue || prevMarkerMain.YValue != sem.YValue)))
            //{
                sem.Label.Text = x.ToString("0") + " ; " + y.ToString("0.0000");
                sem.Label.HorizontalAlign = AlignmentHorizontal.Center;
                sem.Label.Font = new WpfFont("Segoe UI", 11f, true, false);
                sem.Label.Shadow.ContrastColor = Colors.Black;
                sem.Label.VerticalAlign = AlignmentVertical.Top;
                sem.Symbol.Color1 = Colors.Orange;
                sem.Symbol.Height = 12;
                sem.Symbol.Width = 12;
                sem.Symbol.Shape = SeriesMarkerPointShape.Circle;

                prevMarkerMain = sem;
                //return sem;
            //}
            return sem;
        }


        private SeriesEventMarker CreateMouseMarkerMax(double x, double y, SeriesBaseXY owner)
        {
            SeriesEventMarker sem = new SeriesEventMarker(owner);
            sem.XValue = x;
            sem.YValue = y;

            //if ((!(prevMarkerMax != null) || (prevMarkerMax.XValue != sem.XValue || prevMarkerMax.YValue != sem.YValue)))
            //{
                sem.Label.Text = x.ToString("0") + " ; " + y.ToString("0.0000");
                sem.Label.HorizontalAlign = AlignmentHorizontal.Center;
                sem.Label.Font = new WpfFont("Segoe UI", 9f, true, false);
                sem.Label.Shadow.ContrastColor = Colors.Black;
                sem.Label.VerticalAlign = AlignmentVertical.Top;
                sem.Symbol.Color1 = Colors.Aqua;
                sem.Symbol.Height = 10;
                sem.Symbol.Width = 10;
                sem.Symbol.Shape = SeriesMarkerPointShape.Circle;

                prevMarkerMax = sem;
                //return sem;
            //}
            return sem;
        }
        #endregion


        #region update charts
        //drawing points to charts
        private void UpDateChart(byte channel, List<double> dataList)
        {
            try
            {
                CheckMaxValues(channel, dataList);
                CheckMinValues(channel, dataList);

                chartArray[channel - 1].BeginUpdate();
                chartArray[channel - 1].ViewXY.PointLineSeries[0].Points = new SeriesPoint[dataList.Count];

                if (isTransposed)
                {
                    chartArray[channel - 1].ViewXY.XAxes[0].SetRange(-dataList.Count / 2, dataList.Count / 2);
                    int i = 0;
                    for (int j = -dataList.Count / 2; j < dataList.Count / 2; j++)
                    {
                        chartArray[channel - 1].ViewXY.PointLineSeries[0].Points[i] = new SeriesPoint(j, dataList[i]);
                        if (isMaxholdActive)
                        {
                            chartArray[channel - 1].ViewXY.PointLineSeries[1].Points[i] = new SeriesPoint(j, maxHoldPoints[channel - 1][i]);
                        }
                        if (isMinholdActive)
                        {
                            chartArray[channel - 1].ViewXY.PointLineSeries[2].Points[i] = new SeriesPoint(j, minHoldPoints[channel - 1][i]);
                        }
                        i++;
                    }
                }
                else
                {
                    chartArray[channel - 1].ViewXY.XAxes[0].SetRange(0, dataList.Count);
                    for (int j = 0; j < dataList.Count; j++)
                    {
                        chartArray[channel - 1].ViewXY.PointLineSeries[0].Points[j] = new SeriesPoint(j, dataList[j]);
                        if (isMaxholdActive)
                        {
                            chartArray[channel - 1].ViewXY.PointLineSeries[1].Points[j] = new SeriesPoint(j, maxHoldPoints[channel - 1][j]);
                        }
                        if (isMinholdActive)
                        {
                            chartArray[channel - 1].ViewXY.PointLineSeries[2].Points[j] = new SeriesPoint(j, minHoldPoints[channel - 1][j]);
                        }
                    }
                }

                chartArray[channel - 1].EndUpdate();


                if (isAutoFitView)
                {
                    FitView();
                }

                //for (int i = 0; i < NumOfCharts; i++)
                //    chartArray[i].ViewXY.XAxes..ZoomToFit();

                if (channel != 3)
                {
                    requestThread = new Thread(new ThreadStart(StartTimer));
                    requestThread.IsBackground = false;
                    requestThread.Start();
                }
            }
            catch (Exception e)
            { }
        }



        private void FitView()
        {
            for (int i = 0; i < NumOfCharts; i++)
                chartArray[i].ViewXY.ZoomToFit();
        }


        private void CheckMaxValues(byte channel, List<double> dataList)
        {
            if (!isMaxholdActive)
            {
                return;
            }

            chartArray[channel - 1].ViewXY.PointLineSeries[1].Points = new SeriesPoint[dataList.Count];
            if (maxHoldPoints[channel - 1].Count == 0)
            {
                maxHoldPoints[channel - 1] = new List<double>(dataList);
                //maxHoldPoints[channel - 1].Capacity = dataList.Count;
            }

            if (maxHoldPoints[channel - 1].Capacity != dataList.Count)
            {
                maxHoldPoints[channel - 1].Capacity = dataList.Count;
            }

            for (int j = 0; j < dataList.Count; j++)
            {
                var temp = Math.Max(dataList[j], maxHoldPoints[channel - 1][j]);
                maxHoldPoints[channel - 1][j] = temp;
            }
        }


        private void CheckMinValues(byte channel, List<double> dataList)
        {
            chartArray[channel - 1].ViewXY.PointLineSeries[2].Points = new SeriesPoint[dataList.Count];
            if (minHoldPoints[channel - 1].Count == 0)
            {
                minHoldPoints[channel - 1] = new List<double>(dataList);
            }

            if (minHoldPoints[channel - 1].Capacity != dataList.Count)
            {
                minHoldPoints[channel - 1].Capacity = dataList.Count;
            }

            for (int j = 0; j < dataList.Count; j++)
            {
                var temp = Math.Min(dataList[j], minHoldPoints[channel - 1][j]);
                minHoldPoints[channel - 1][j] = temp;
                //chartArray[channel - 1].ViewXY.PointLineSeries[2].Points[j] = new SeriesPoint(j, minHoldPoints[channel - 1][j]);
            }
        }
        #endregion




        private void ResetAccumulations()
        {
            for (int i = 0; i < NumOfCharts; i++)
            {
                maxHoldPoints[i].Clear();
                chartArray[i].ViewXY.PointLineSeries[1].Points = new SeriesPoint[0];
                chartArray[i].ViewXY.PointLineSeries[1].SeriesEventMarkers.Clear(); 
                minHoldPoints[i].Clear();
                chartArray[i].ViewXY.PointLineSeries[2].Points = new SeriesPoint[0];
                chartArray[i].ViewXY.PointLineSeries[2].SeriesEventMarkers.Clear();
                savedLastPoints[i].Clear();
            }
        }


        private void CleanCharts()
        {
            for (int i = 0; i < NumOfCharts; i++)
            {
                chartArray[i].ViewXY.PointLineSeries[0].Points = new SeriesPoint[0];
                chartArray[i].ViewXY.PointLineSeries[1].Points = new SeriesPoint[0];
                chartArray[i].ViewXY.PointLineSeries[2].Points = new SeriesPoint[0];
            }
            ResetAccumulations();
        }

        private void InitCharts()
        {
            chartArray = new LightningChartUltimate[NumOfCharts] { CreateChart(gridChart), CreateChart(gridChart2), CreateChart(gridChart3) };
            savedLastPoints = new List<double>[NumOfCharts];
             maxHoldPoints = new List<double>[NumOfCharts];
            minHoldPoints = new List<double>[NumOfCharts];
            for (int i = 0; i < NumOfCharts; i++)
            {
                savedLastPoints[i] = new List<double>();
                maxHoldPoints[i] = new List<double>();
                minHoldPoints[i] = new List<double>();
            }
        }
    }
}
